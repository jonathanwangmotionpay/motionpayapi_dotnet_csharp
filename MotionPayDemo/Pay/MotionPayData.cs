﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Net;
using System.Security.Cryptography;
using Newtonsoft.Json;

using TuYu.Common;
namespace MotionPayDemo.Pay
{
    class BaseParam
    {
        public string GetQueryString()
        {
            var properties = from p in this.GetType().GetProperties()
                             where p.GetValue(this, null) != null
                             select p.Name.ToLower() + "=" + p.GetValue(this, null).ToString();

            return String.Join("&", properties.ToArray());
        }
    }
    class QRcodeRequest : BaseParam
    {
        public string goods_info
        {
            get;
            set;
        }
        public string mid
        {
            get;
            set;
        }
        public string out_trade_no
        {
            get;
            set;
        }
        public string pay_channel
        {
            get;
            set;
        }
        public string return_url
        {
            get;
            set;
        }
        public string sign
        {
            get;
            set;
        }
        public string spbill_create_ip
        {
            get;
            set;
        }
        public string terminal_no
        {
            get;
            set;
        }
        public string total_fee
        {
            get;
            set;
        }
    }

    class QueryRequest : BaseParam
    {
        public string mid
        {
            get;
            set;
        }
        public string out_trade_no
        {
            get;
            set;
        }
        public string sign
        {
            get;
            set;
        }
    }

    class RefundCancelRequest : BaseParam
    {
        public string mid
        {
            get;
            set;
        }
        public string out_trade_no
        {
            get;
            set;
        }
        public int refund_amount
        {
            get;
            set;
        }
        public string sign
        {
            get;
            set;
        }
        public int total_fee
        {
            get;
            set;
        }
        /*
        public string tranCode
        {
            get;
            set;
        }
        public string tranLogId
        {
            get;
            set;
        }
        */
    }

    class Content
    {
        public string qrcode
        {
            get;
            set;
        }
        public string out_trade_no
        {
            get;
            set;
        }
        public string pay_channel
        {
            get;
            set;
        }
        public string total_fee
        {
            get;
            set;
        }
        public string trade_status
        {
            get;
            set;
        }
        public string transaction_id
        {
            get;
            set;
        }
        public string currency_type
        {
            get;
            set;
        }
        public string exchange_rate
        {
            get;
            set;
        }
        public string settlement_amount
        {
            get;
            set;
        }
        public string user_identify
        {
            get;
            set;
        }
        public string refund_fee
        {
            get;
            set;
        }
        public string refund_status
        {
            get;
            set;
        }
    }
    class Response
    {
        public string code
        {
            get;
            set;
        }
        public string message
        {
            get;
            set;
        }
        public Content content
        {
            get;
            set;
        }
    }
    class OtherResponse
    {
        public string code
        {
            get;
            set;
        }
        public string message
        {
            get;
            set;
        }
        public Content content
        {
            get;
            set;
        }
    }
    class MotionPayData
    {
        private string GetSignParams(MotionPayAPI theMotionPayAPI)
        {
            return "&appid=" + theMotionPayAPI.getAppId() + "&appsecret=" + theMotionPayAPI.getAppSecure();
        }

        private string GetSHA1HashData(string data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            byte[] hashData = sha.ComputeHash(Encoding.UTF8.GetBytes(data));

            return BitConverter.ToString(hashData).Replace("-", "").ToUpper();
        }

        public string verifySign(string queryStr, MotionPayAPI theMotionPayAPI)
        {
            //the stringSignTemp Should be: amount=1&authcode=134583657133882637&merchantorderno=2018081106176844&paramjsonobject={"goods_info":"Test_Product","spbill_create_ip":"192.168.2.253","store_id":"","terminal_no":"MyPosT00001"}&paychannel=U&appid=5005642017008&appsecret=cd3f5e88a1ec1df2351cdca75d7ce94a
            string stringSignTemp = queryStr + this.GetSignParams(theMotionPayAPI);
            Log.WriteLog("motionpay", "stringSignTemp is:" + stringSignTemp);
            // string sign = GetSHA1HashData("amount=1&authcode=134583657133882637&merchantorderno=2018081106176844&paramjsonobject={\"goods_info\":\"Test_Product\",\"spbill_create_ip\":\"192.168.2.253\",\"store_id\":\"\",\"terminal_no\":\"MyPosT00001\"}&paychannel=U&appid=5005642017008&appsecret=cd3f5e88a1ec1df2351cdca75d7ce94a");
            string sign = GetSHA1HashData(stringSignTemp);
            Log.WriteLog("motionpay", "sign is:" + sign);
            return sign;
        }

        public string getSign(BaseParam theParam, MotionPayAPI theMotionPayAPI)
        {
            //the stringSignTemp Should be: amount=1&authcode=134583657133882637&merchantorderno=2018081106176844&paramjsonobject={"goods_info":"Test_Product","spbill_create_ip":"192.168.2.253","store_id":"","terminal_no":"MyPosT00001"}&paychannel=U&appid=5005642017008&appsecret=cd3f5e88a1ec1df2351cdca75d7ce94a
            string stringSignTemp = theParam.GetQueryString() + this.GetSignParams(theMotionPayAPI);
            Log.WriteLog("motionpay","stringSignTemp is:" + stringSignTemp);
            // string sign = GetSHA1HashData("amount=1&authcode=134583657133882637&merchantorderno=2018081106176844&paramjsonobject={\"goods_info\":\"Test_Product\",\"spbill_create_ip\":\"192.168.2.253\",\"store_id\":\"\",\"terminal_no\":\"MyPosT00001\"}&paychannel=U&appid=5005642017008&appsecret=cd3f5e88a1ec1df2351cdca75d7ce94a");
            string sign = GetSHA1HashData(stringSignTemp);
            Log.WriteLog("motionpay","sign is:" + sign);
            return sign;
        }

        public string GetPaymentStateString(int paymentState)
        {
            string stateString = "";
            if (paymentState > 0)
            {
                if (paymentState == 1)
                {
                    stateString = paymentState + " - Paying";
                }
                if (paymentState == 2)
                {
                    stateString = paymentState + " - Paid";
                }
                if (paymentState == 3)
                {
                    stateString = paymentState + " - Refund";
                }
                if (paymentState == 4)
                {
                    stateString = paymentState + " - Closed";
                }
                if (paymentState == 5)
                {
                    stateString = paymentState + " - Cancelled";
                }
            }
            return stateString;
        }

    }
}