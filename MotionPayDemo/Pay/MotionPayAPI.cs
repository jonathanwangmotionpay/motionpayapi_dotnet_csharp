﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

using System.IO;
using System.Web;
using System.Net;
using System.Security.Cryptography;
using Newtonsoft.Json;

namespace MotionPayDemo.Pay
{
    class MotionPayAPI
    {
        private bool usingLiveServer = false;
        private string API_HOST_URL_TESTING = "https://api.motionpay.org/";
        private string API_HOST_URL_LIVE = "APIHOST_To_Be_Replaced_Before_Live";

        private string mid_TEST = "100105100000011";
        private string appId_TEST = "5005642017006";
        private string appSecure_TEST = "2ce9f51261e21ba6a087ee239160f0b1";

        private string mid_LIVE = "MID_To_Be_Replaced_Before_Live";
        private string appId_LIVE = "APPID_To_Be_Replaced_Before_Live";
        private string appSecure_LIVE = "APPSECURE_To_Be_Replaced_Before_Live";

        /**
          * offline POS machine API action target for order
          */
        private string ACTION_API_PRE_PAY = "/onlinePayment/v1_1/pay/prePay";
        /**
         * offline POS machine API action target for query
         */
        private string ACTION_API_QUERY_ORDER = "/onlinePayment/v1_1/pay/orderQuery";
        /**
         * offline POS machine API action target for refund
         */
        private string ACTION_API_REVOKE = "/onlinePayment/v1_1/pay/revoke";
        /**
         * offline POS machine API action target for cancel
         */
        private string ACTION_API_CANCEL = "/onlinePayment/v1_1/pay/cancel";

        private string returnHostURL = "http://win.motionpay.org/";
        private string appPath = "";

        public void SetUsingLiveServerFlag(bool usingLive)
        {
            usingLiveServer = usingLive;
        }

        public void setLiveMerchantId(string mid)
        {
            mid_LIVE = mid;
        }

        public void setLiveAppId(string appId)
        {
            appId_LIVE = appId;
        }

        public void setLiveAppSecure(string appSecure)
        {
            appSecure_LIVE = appSecure;
        }

        public void setReturnHostURL(string url)
        {
            returnHostURL = url;
        }

        public void setAppPath(string path)
        {
            appPath = path;
        }

        private string GetServerApiHostURL()
        {
            string url = API_HOST_URL_TESTING;
            if (usingLiveServer)
            {
                url = API_HOST_URL_LIVE;
            }
            return url;
        }

        private string GetPrePayServerActionURL()
        {
            return GetServerApiHostURL() + ACTION_API_PRE_PAY;
        }

        private string GetQueryServerActionURL()
        {
            return GetServerApiHostURL() + ACTION_API_QUERY_ORDER;
        }

        private string GetRevokeServerActionURL()
        {
            return GetServerApiHostURL() + ACTION_API_REVOKE;
        }

        private string GetCancelServerActionURL()
        {
            return GetServerApiHostURL() + ACTION_API_CANCEL;
        }

        public string getMid()
        {
            string mid = mid_TEST;
            if (usingLiveServer)
            {
                mid = mid_LIVE;
            }
            return mid;
        }

        public string getAppId()
        {
            string appId = appId_TEST;
            if (usingLiveServer)
            {
                appId = appId_LIVE;
            }
            return appId;
        }

        public string getAppSecure()
        {
            string appSecure = appSecure_TEST;
            if (usingLiveServer)
            {
                appSecure = appSecure_LIVE;
            }
            return appSecure;
        }

        private string PostData(string uri, string jsonRequest)
        {
            var http = (HttpWebRequest)WebRequest.Create(new Uri(uri));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";

            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] bytes = encoding.GetBytes(jsonRequest);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();

            return content;
        }

        public string SendQRCodeRequest(string jsonRequest)
        {
            string theResult = PostData(GetPrePayServerActionURL(), jsonRequest);
            return theResult;
        }

        public string SendQueryRequest(string jsonRequest)
        {
            string theResult = PostData(GetQueryServerActionURL(), jsonRequest);
            return theResult;
        }

        public string SendRefundRequest(string jsonRequest)
        {
            string theResult = PostData(GetRevokeServerActionURL(), jsonRequest);
            return theResult;
        }

        public string SendCancelRequest(string jsonRequest)
        {
            string theResult = PostData(GetCancelServerActionURL(), jsonRequest);
            return theResult;
        }
    }
}