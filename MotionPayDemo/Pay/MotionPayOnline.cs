﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
using System.Net;
using System.Security.Cryptography;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;

using Newtonsoft.Json;
using ZXing;
using ZXing.QrCode;

using TuYu.Common;
namespace MotionPayDemo.Pay
{
    public class MotionPayOnline
    {
        MotionPayData theMotionPayData = new MotionPayData();
        MotionPayAPI theMotionPayAPI = new MotionPayAPI();

        public string init(string commandStr)
        {
            theMotionPayAPI.SetUsingLiveServerFlag(false);
            string msg = "";
            return msg;
        }

        public void setUsingLiveServerFlag(bool flag)
        {
            theMotionPayAPI.SetUsingLiveServerFlag(flag);
        }

        public void setLiveMerchantId(string mid)
        {
            theMotionPayAPI.setLiveMerchantId(mid);
        }

        public void setLiveAppId(string appId)
        {
            theMotionPayAPI.setLiveAppId(appId);
        }

        public void setLiveAppSecure(string appSecure)
        {
            theMotionPayAPI.setLiveAppSecure(appSecure);
        }

        public void GenerateMyQCCode(string QCText, string path)
        {
            var QCwriter = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                  {
                      Width = 200,
                      Height = 200
                  }
            };
            var result = QCwriter.Write(QCText);
            var barcodeBitmap = new Bitmap(result);

            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(path,
                   FileMode.Create, FileAccess.ReadWrite))
                {
                    barcodeBitmap.Save(memory, ImageFormat.Jpeg);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
        }

        public string getSignForVerify(string queryStr)
        {
            return theMotionPayData.verifySign(queryStr, theMotionPayAPI);
        }

        public Hashtable doQRcode(Hashtable parameters)
        {
            Hashtable result = new Hashtable();
            QRcodeRequest theQRcodeRequest;
            Response responseObj;
            string debug_info = "";
            string payChannelParam = "";

            string paymentTypeParam = (string)parameters["paymentType"];
            string goods_infoParam = (string)parameters["goods_info"];
            string spbill_create_ipParam = (string)parameters["spbill_create_ip"];
            string store_idParam = (string)parameters["store_id"];
            string terminal_noParam = (string)parameters["terminal_no"];
            string amountParam = (string)parameters["amount"];
            string merchantOrderNoParam = (string)parameters["out_trade_no"];
            string returnURLParam = (string)parameters["returnURL"];
            goods_infoParam = goods_infoParam.Replace(" ", "_");

            // these are default values
            if (paymentTypeParam == null || paymentTypeParam.Length == 0)
            {
                payChannelParam = "A";
            }
            else if (paymentTypeParam.CompareTo("A") == 0)
            {
                payChannelParam = "A";
            }
            else if (paymentTypeParam.CompareTo("W") == 0)
            {
                payChannelParam = "W";
            }
                
            if (goods_infoParam == null || goods_infoParam.Length == 0)
                goods_infoParam = "Merchant_Product";
            if (spbill_create_ipParam == null || spbill_create_ipParam.Length == 0)
                spbill_create_ipParam = "192.168.1.1";
            if (store_idParam == null || store_idParam.Length == 0)
                store_idParam = "store001";
            if (terminal_noParam == null || terminal_noParam.Length == 0)
                terminal_noParam = "WebServer";
            if (amountParam == null || amountParam.Length == 0)
                amountParam = "1";
            if (merchantOrderNoParam == null || merchantOrderNoParam.Length == 0)
                merchantOrderNoParam = DateTime.Now.ToString("yyyyMMddHHmmssfff");

            // merchantOrderNoParam = "2019053018111511";
            // returnURLParam = "https://demo.motionpay.org/motionpayAPI_JAVA_PUB/CallbackServlet";
            theQRcodeRequest = new QRcodeRequest
            {
                out_trade_no = merchantOrderNoParam,
                goods_info = goods_infoParam,
                total_fee = amountParam,
                pay_channel = payChannelParam,
                return_url = returnURLParam,
                spbill_create_ip = spbill_create_ipParam,
                mid = theMotionPayAPI.getMid(),
                terminal_no = terminal_noParam,
                sign = null   // temp sign from docuemnt
            };

            Log.WriteLog("motionpay", "theQRcodeRequest is:" + theQRcodeRequest);
            theQRcodeRequest.sign = theMotionPayData.getSign(theQRcodeRequest, theMotionPayAPI);
            // Serialize it.  
            string serializedJson = JsonConvert.SerializeObject(theQRcodeRequest);

            debug_info = debug_info + "request serializedJson:" + serializedJson + "\n";
            string response = "";
            response = theMotionPayAPI.SendQRCodeRequest(serializedJson);
            debug_info = debug_info + "response:" + response + "\n";
            Log.WriteLog("motionpay", "debug_info is:" + debug_info);

            responseObj = JsonConvert.DeserializeObject<Response>(response);
            result.Add("response_message", responseObj.message);
            // debug_info = debug_info + "responseMessage:" + responseObj.message + "\n";
            result.Add("code", responseObj.code);
            if (String.Compare(responseObj.code, "0") == 0)
            {
                debug_info = debug_info + "The payment request has been sucessfully submitted." + "\n";
                if (responseObj.content != null && responseObj.content.qrcode != null)
                {
                    result.Add("qrcode", responseObj.content.qrcode);
                    Log.WriteLog("motionpay", "qrcode is:" + responseObj.content.qrcode);
                }
                result.Add("message", "The payment request has been sucessfully submitted.");
            }
            else
            {
                debug_info = debug_info + "There is an error in the response. The error code is:" + responseObj.code + "\n";
                result.Add("message", "There is an error in the response. The error code is:" + responseObj.code + "\n");
            }
            result.Add("debug_info", debug_info);
            Log.WriteLog("motionpayOrders", "outTradeNo:" + merchantOrderNoParam + ",paymentType:" + paymentTypeParam + "\n");

            return result;
        }

        public Hashtable doQuery(Hashtable parameters)
        {
            Hashtable result = new Hashtable();
            QueryRequest theQueryRequest;
            OtherResponse queryResponseObj;
            string debug_info = "";

            theQueryRequest = new QueryRequest
            {
                mid = theMotionPayAPI.getMid(),
                out_trade_no = (string)parameters["out_trade_no"],
                sign = null
            };
            theQueryRequest.sign = theMotionPayData.getSign(theQueryRequest, theMotionPayAPI);
            // Serialize it.  
            string serializedJson = JsonConvert.SerializeObject(theQueryRequest);
            Log.WriteLog("motionpay", "request is:" + serializedJson);

            // Print on the screen.  
            debug_info = debug_info + "request serializedJson:" + serializedJson + "\n";
            string response = theMotionPayAPI.SendQueryRequest(serializedJson);
            debug_info = debug_info + "response:" + response + "\n";
            Log.WriteLog("motionpay", "response is:" + response);

            queryResponseObj = JsonConvert.DeserializeObject<OtherResponse>(response);
            result.Add("response_message", queryResponseObj.message);
            result.Add("code", queryResponseObj.code);
            if (String.Compare(queryResponseObj.code, "0") == 0)
            {
                debug_info = debug_info + "The query request has been sucessfully submitted." + "\n";
                result.Add("message", "The query request has been sucessfully submitted.");
                if (queryResponseObj.content != null && queryResponseObj.content.trade_status != null)
                {
                    result.Add("trade_status", queryResponseObj.content.trade_status);
                    result.Add("total_fee", queryResponseObj.content.total_fee);
                    result.Add("pay_channel", queryResponseObj.content.pay_channel);
                    result.Add("currency_type", queryResponseObj.content.currency_type);
                    result.Add("exchange_rate", queryResponseObj.content.exchange_rate);
                    result.Add("user_identify", queryResponseObj.content.user_identify);
                }
            }
            else
            {
                debug_info = debug_info + "There is an error in the query response." + "\n";
                result.Add("message", "There is an error in the query response.");
            }
            result.Add("debug_info", debug_info);
            return result;
        }

        public Hashtable doRefund(Hashtable parameters)
        {
            Hashtable result = new Hashtable();
            QueryRequest theQueryRequest;
            OtherResponse queryResponseObj;
            RefundCancelRequest theRefundCancelRequest;
            OtherResponse refundCancelResponseObj;
            string debug_info = "";

            theQueryRequest = new QueryRequest
            {
                mid = theMotionPayAPI.getMid(),
                out_trade_no = (string)parameters["out_trade_no"],
                sign = null
            };
            theQueryRequest.sign = theMotionPayData.getSign(theQueryRequest, theMotionPayAPI);
            // Serialize it.  
            string serializedJson = JsonConvert.SerializeObject(theQueryRequest);
            string response = theMotionPayAPI.SendQueryRequest(serializedJson);
            queryResponseObj = JsonConvert.DeserializeObject<OtherResponse>(response);

            int i_refundAmount = 1;  // set to the right amount later
            string s_refundAmountParam = (string)parameters["refund_amount"];
            if (s_refundAmountParam != null && s_refundAmountParam.Length > 0)
            {
                i_refundAmount = (int)(float.Parse(s_refundAmountParam) * 100);
            }
            int i_total_fee = 1; // set to the right amount later
            string s_totalAmountParam = (string)parameters["totalAmount"];
            if (s_totalAmountParam != null && s_totalAmountParam.Length > 0)
            {
                i_total_fee = (int)(float.Parse(s_totalAmountParam) * 100);
            }

            theRefundCancelRequest = new RefundCancelRequest
            {
                mid = theMotionPayAPI.getMid(),
                out_trade_no = queryResponseObj.content.out_trade_no,
                refund_amount = i_refundAmount,  // partial refund is supported
                total_fee = i_total_fee,
                sign = null
                // we don't need these parameters any more
                // tranCode = queryResponseObj.result.tranCode,
                // tranLogId = queryResponseObj.result.tranLogId
            };
            theRefundCancelRequest.sign = theMotionPayData.getSign(theRefundCancelRequest, theMotionPayAPI);
            // Serialize it.  
            serializedJson = JsonConvert.SerializeObject(theRefundCancelRequest);
            Log.WriteLog("motionpay", "refund request is:" + serializedJson);

            debug_info = debug_info + "request serializedJson:" + serializedJson + "\n";
            response = theMotionPayAPI.SendRefundRequest(serializedJson);
            Log.WriteLog("motionpay", "refund response is:" + response);
            debug_info = debug_info + "response:" + response + "\n";

            refundCancelResponseObj = JsonConvert.DeserializeObject<OtherResponse>(response);
            result.Add("response_message", refundCancelResponseObj.message);
            result.Add("code", refundCancelResponseObj.code);
            if (String.Compare(refundCancelResponseObj.code, "0") == 0)
            {
                debug_info = debug_info + "The refund request has been sucessfully submitted." + "\n";
                result.Add("message", "The refund request has been sucessfully submitted.");
                result.Add("total_fee", refundCancelResponseObj.content.total_fee);
                result.Add("refund_fee", refundCancelResponseObj.content.refund_fee);
                result.Add("pay_channel", refundCancelResponseObj.content.pay_channel);
                result.Add("refund_status", refundCancelResponseObj.content.refund_status);
            }
            else
            {
                debug_info = debug_info + "The refund failed. There is an error in the response." + "\n";
                result.Add("message", "There is an error in the response. The error code is: " + refundCancelResponseObj.code);
            }
            result.Add("debug_info", debug_info);
            return result;
        }

        public Hashtable doCancel(Hashtable parameters)
        {
            Hashtable result = new Hashtable();
            QueryRequest theQueryRequest;
            OtherResponse queryResponseObj;
            RefundCancelRequest theRefundCancelRequest;
            OtherResponse refundCancelResponseObj;
            string debug_info = "";

            theQueryRequest = new QueryRequest
            {
                mid = theMotionPayAPI.getMid(),
                out_trade_no = (string)parameters["out_trade_no"],
                sign = null
            };
            theQueryRequest.sign = theMotionPayData.getSign(theQueryRequest, theMotionPayAPI);
            // Serialize it.  
            string serializedJson = JsonConvert.SerializeObject(theQueryRequest);
            string response = theMotionPayAPI.SendQueryRequest(serializedJson);
            queryResponseObj = JsonConvert.DeserializeObject<OtherResponse>(response);

            theRefundCancelRequest = new RefundCancelRequest
            {
                // out_trade_no = queryResponseObj.result.out_trade_no,
                // tranCode = queryResponseObj.result.tranCode,
                // tranLogId = queryResponseObj.result.tranLogId,
                sign = null
            };
            theRefundCancelRequest.sign = theMotionPayData.getSign(theRefundCancelRequest, theMotionPayAPI);
            // Serialize it.  
            serializedJson = JsonConvert.SerializeObject(theRefundCancelRequest);

            debug_info = debug_info + "request serializedJson:" + serializedJson + "\n";
            response = theMotionPayAPI.SendCancelRequest(serializedJson);
            debug_info = debug_info + "response:" + response + "\n";

            refundCancelResponseObj = JsonConvert.DeserializeObject<OtherResponse>(response);
            result.Add("response_message", refundCancelResponseObj.message);
            result.Add("code", refundCancelResponseObj.code);
            if (String.Compare(refundCancelResponseObj.code, "0") == 0)
            {
                debug_info = debug_info + "The cancel request has been sucessfully submitted." + "\n";
                result.Add("message", "The cancel request has been sucessfully submitted.");
            }
            else
            {
                debug_info = debug_info + "The refund failed. There is an error in the response." + "\n";
                result.Add("message", "There is an error in the response. The error code is: " + refundCancelResponseObj.code);
            }
            result.Add("debug_info", debug_info);
            return result;
        }
    }
}