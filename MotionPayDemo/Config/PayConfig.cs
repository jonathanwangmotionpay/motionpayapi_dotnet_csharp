﻿/* Developer Jonathan  */
/* Email: jonathan.wang@motionpay.ca  */
namespace MotionPay.Config
{
    /// <summary>
    /// 支付相关参数配置
    /// </summary>
    public class PayConfig
    {
        /* CAD parameters  live */

        public static string AppId = "wx8ddacf4e1347f9c8";
        public static string AppSecret = "565ccc5a1fdb8e9873b7244b4b044935";
        public static string WECHAT_MINI_MID = "100100010000055";
        public static string WECHAT_MINI_APPID = "5005642018019";
        public static string WECHAT_MINI_APPSECURE = "c7d79e0075076e72c5f779fc62f0f4e9";
        public static string Notify_Url = "http://win.motionpay.org/callback.aspx";

        public static string BASE_URL = "https://online.motionpaytech.com/onlinePayment/v1_1/pay";
        public static string ORDER_MINI_URL = BASE_URL + "/mini";

        private static string apiHostURL = "https://online.motionpaytech.com/";
        /// <summary>
        /// 支付宝H5-请求地址
        /// </summary>
        public static string Alipay_H5_PAY = apiHostURL + "onlinePayment/v1_1/pay/wapPay";
        /// <summary>
        /// 支付宝H5-结果跳转地址
        /// </summary>
        public static string Alipay_PAY_URL = apiHostURL + "onlinePayment/v1_1/pay/getPayUrl";
        /// <summary>
        /// 支付宝H5--回调
        /// </summary>
        public static string Alipay_Notify_Url = "";
    }
}
