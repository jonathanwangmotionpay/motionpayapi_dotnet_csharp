﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="payment.aspx.cs" Inherits="MotionPayDemo.payment" %>
<%@ Import Namespace="TuYu.Common" %>
<%@ Import Namespace="MotionPayDemo.Pay" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Page</title>

	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>

<script type="text/javascript">
var myTimeoutVar;
var timeoutCount = 5000;    // online query is every 5 seconds
var countDownTimer = 300;	// 300 seconds to pay

<%
    String imageHTMLCode = "";
    String paymentType = "", type = "";
    String orderId = "";
    String paymentAmount = "1";

    String serverName = Request.Url.Host;
    int serverPort = Request.Url.Port;
    String url = "https://" + serverName + "/";
    Log.WriteLog("motionpay", "URL is:" + url);
    String callbackURL = url + "callback.aspx";

    String appPath = HttpContext.Current.Request.ApplicationPath;
    String demoServerURL = url + appPath + "/";
    Log.WriteLog("motionpay", "demoServerURL is:" + demoServerURL);
    Random random = new Random();
    long id = random.Next(99999999);

    String errorMsg = "";
    orderId = DateTime.Now.ToString("yyyyMMdd") + id.ToString("D8");
    String terminalNo = "WebServer";
    String amountInReq = Request["paymentAmount"];

    if (amountInReq != null && amountInReq.Length > 0)
    {
        try
        {
            float amountInFloat = float.Parse(amountInReq) * 100;
            int valueInt = (int)amountInFloat;
            paymentAmount = valueInt.ToString();
            Log.WriteLog("motionpay", "paymentAmount is:" + paymentAmount);
        }
        catch (Exception ee)
        {
            Log.WriteLog("motionpay", "Exception is:" + ee);
        }
    }
    String productName = "Test_Product";

    type = Request["paymentInfo.bankSelect"];
    if (type == null)
        type = "";
    String scanImageHtmlCode = "";
    paymentType = type;
    Log.WriteLog("motionpay", "the paymentType is:" + type);

    if (type.CompareTo("W") == 0)
    {
        scanImageHtmlCode = "<img src='images/wechatpay.png'/>";
    }
    else if (type.CompareTo("A") == 0)
    {
        scanImageHtmlCode = "<img src='images/alipay.png'/>";
    }
    else if (type.CompareTo("H5_A") == 0)
    {
        scanImageHtmlCode = "<img src='images/alipayH5.png'/>";
    }
    else if (type.CompareTo("H5_W") == 0)
    {
        scanImageHtmlCode = "<img src='images/wechatH5.png'/>";
    }
    else if (type.CompareTo("SM_A") == 0)
    {
        scanImageHtmlCode = "<img src='images/scanMobileA.png'/>";
    }
    else if (type.CompareTo("SM_W") == 0)
    {
        scanImageHtmlCode = "<img src='images/scanMobileW.png'/>";
    }
    else if (type.CompareTo("SM_PU") == 0)
    {
        scanImageHtmlCode = "<img src='images/scanMobilePU.png'/>";
    }
    else if (type.CompareTo("QR_PA") == 0)
    {
        scanImageHtmlCode = "<img src='images/alipayQR_PA.png'/>";
    }
    else if (type.CompareTo("QR_PW") == 0)
    {
        scanImageHtmlCode = "<img src='images/wechatQR_PW.png'/>";
    }
    else
    {
        errorMsg = "We can only support Wechat and Alipay for now.";
    }

    Hashtable requestParams = new Hashtable();
    requestParams.Add("paymentType", paymentType);
    requestParams.Add("out_trade_no", orderId);
    requestParams.Add("terminalNo", terminalNo);
    requestParams.Add("paymentAmount", paymentAmount);
    requestParams.Add("goods_info", productName);
    requestParams.Add("returnURL", callbackURL);

    MotionPayOnline onlineAPI = new MotionPayOnline();

    Hashtable result = onlineAPI.doQRcode(requestParams);
    string qrcode = (string)result["qrcode"];
    string path = Server.MapPath("~/image/QRImage" + orderId + ".jpg");
    string imagePath = "image/QRImage" + orderId + ".jpg";
    onlineAPI.GenerateMyQCCode(qrcode, path);
    imageHTMLCode = scanImageHtmlCode;
    Log.WriteLog("motionpay", "errorMsg is:" + errorMsg);
    
if(paymentType != null && (type.CompareTo("QR_PA") == 0 || type.CompareTo("QR_PW") == 0) ) {
%>	
	timeoutCount = 30000;   // POS query is every 30 seconds
	setTimeout(showCountDownCancelDiv, 500);
<%	
}
%>

function updateCountDownClock() {
	// alert("updateCountDownClock");
	var divCountDown = document.getElementById("countDown");
	if(divCountDown != null) {
		// alert("count down seconds!");
		divCountDown.innerHTML = countDownTimer + "";
		countDownTimer = countDownTimer - 1;
		if(countDownTimer >= 0) {
			setTimeout(updateCountDownClock, 1000);
		}
		else {
			cancelOrder();
		}
	}
}

function showCountDownCancelDiv() {
	// alert("showCountDownCancelDiv");
	var divForCancel = document.getElementById("countDownCancelDiv");
	if(divForCancel != null) {
		// alert("show the div.");
		divForCancel.style.display = "block";
		updateCountDownClock();
	}
	else {
		// alert("div is null.");
	}
}

function orderHasBeenCancelled() {
	var divForCancel = document.getElementById("countDownCancelDiv");
	if(divForCancel != null) {
		// alert("show the div.");
		divForCancel.style.display = "none";
	}
	var divToUpdate = document.getElementById("infor_box");
	divToUpdate.innerHTML = "<br/><font class='cOrange' style='font-size: 25px;'>The order has been cancelled.</font>";	
	clearTimeout(myTimeoutVar);
}

function checkOrder() {
	// alert("checkOrder");
	var checkResult = checkPaymentResult();
	if(checkResult == false) {
		var divOrderStatus = document.getElementById("orderStatus");
		if(divOrderStatus != null) {
			divOrderStatus.innerHTML = "The order is not paid yet. Checked when " + countDownTimer + " seconds are left.";
		}
	}
}

function cancelOrder() {
	// alert("cancelOrder function!");
	var result = false;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "cancelOrder.aspx?orderId=<% Response.Write(orderId); %>&paymentAmount=<% Response.Write(paymentAmount); %>&paymentType=<% Response.Write(paymentType); %>&", true);
	xhr.onload = function (e) {
	  if (xhr.readyState === 4) {
	    if (xhr.status === 200) {
	      var resultStr = xhr.responseText;
	      console.log(resultStr);
	      resultStr = resultStr.trim();
	      // alert("cancelOrder is:#" + resultStr +"#");
	      if(resultStr == "cancelled") {
	    	  result = true;
	    	  orderHasBeenCancelled();
	      }
	    } else {
	      console.error("status error:" + xhr.statusText);
	    }
	  }
	};
	xhr.onerror = function (e) {
	  console.error("function error:" + xhr.statusText);
	};
	if(result == false) {
		xhr.send(null);
	}
	return result;
}

function checkPaymentResult() {
    // alert("check payment result!");
	var paid = false;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "paymentNotify.aspx?orderId=<% Response.Write(orderId); %>&paymentAmount=<% Response.Write(paymentAmount); %>&paymentType=<% Response.Write(paymentType); %>&", true);
	xhr.onload = function (e) {
	  if (xhr.readyState === 4) {
	    if (xhr.status === 200) {
	      var resultStr = xhr.responseText;
	      console.log(resultStr);
	      resultStr = resultStr.trim();
	      // alert("resultStr is:#" + resultStr +"#");
	      if(resultStr == "paid") {
	    	  // alert("payment is done.");
	    	  paid = true;
	    	  var divToUpdate = document.getElementById("infor_box");
	    	  divToUpdate.innerHTML = "<br/><font class='cOrange' style='font-size: 25px;'>Thank you very much for your payment.</font>";
	    	  clearTimeout(myTimeoutVar);
	      }
	    } else {
	      console.error(xhr.statusText);
	    }
	  }
	};
	xhr.onerror = function (e) {
	  console.error(xhr.statusText);
	};
	if(paid == false) {
		xhr.send(null);
		myTimeoutVar = setTimeout(checkPaymentResult, timeoutCount);
	}
	return paid;
}

function setTimeoutCheckFunc() {
    // alert("setTimeoutCheckFunc");
	myTimeoutVar = setTimeout(checkPaymentResult, timeoutCount);
}

<%
if(type.CompareTo("W") == 0 || type.CompareTo("A") == 0 || type.CompareTo("QR_PW") == 0 || type.CompareTo("QR_PA") == 0) {
%>
	setTimeoutCheckFunc();
<%
}


%>

</script>	
  </head>
 <body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="#" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
    <div class="pay_infor"  >
    <p><font class="cOrange" style="font-size: 25px;">Order id: <% Response.Write(orderId); %></font></p>
    <br/>
    <div id="infor_box" class="infor_box" style="height:430px;">
 
 <%     
 	if(errorMsg.Length == 0) {
 		
 		callbackURL = demoServerURL + "CallbackServlet";
 		

	 		// filename = bean.generateQRImageOnline(paymentType, orderId, terminalNo, paymentAmount, productName, callbackURL);
%>
	 		<p><font style="font-size:15px;font-weight:bold;color:#2489c4;">
	 		Please scan this image from you cell phone to pay it:</font></p>
	 		 <br/>
	 		 <%= scanImageHtmlCode %> <br/>
	 			 <img alt="qrcode" src="<% Response.Write(imagePath);  %>"/>
<%			 
 	}
 	else {
        Response.Write(errorMsg);
 	}	
%>
 
    </div>
  </div>
  
 
  </div>

</body>
</html>