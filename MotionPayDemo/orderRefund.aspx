﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orderRefund.aspx.cs" Inherits="MotionPayDemo.orderRefund" %>
<%@ Import Namespace="MotionPayDemo.Pay" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
</script>	
<style>
table, td, th {
    text-align: left;
}
</style>
  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span>Sample Order Refund Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
      <span style="float:right;"><a href="orderList.jsp"><font style="font-size:15px;font-weight:bold;color:#f60;">Order List</font></a></span> 
    </div>
</div>
  
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Order Refund Result</label></h6>
  	  <ul class="sel_list">
<%
    string outTradeNo = Request["orderId"];
    string refundAmount = Request["refundAmount"];
    string totalAmount = Request["totalAmount"];

    
    Hashtable requestParams = new Hashtable();
    MotionPayOnline onlineAPI = new MotionPayOnline();
    requestParams.Add("out_trade_no", outTradeNo);
    requestParams.Add("refundAmount", refundAmount);
    requestParams.Add("totalAmount", totalAmount);
    Hashtable result = onlineAPI.doRefund(requestParams);

    string code = (string)result["code"];
    if (code == "0")
    {
        string amountPaidInCents = (string)result["total_fee"];
        if (amountPaidInCents == null || amountPaidInCents.Length == 0)
        {
            amountPaidInCents = "0";
        }
        float amountInFloat = float.Parse(amountPaidInCents) / 100;
        string amountPaid = amountInFloat.ToString("0.00");
        string amountRefundInCents = (string)result["refund_fee"];
        if (amountRefundInCents == null || amountRefundInCents.Length == 0)
        {
            amountRefundInCents = "0";
        }
        float amountRefundInFloat = float.Parse(amountRefundInCents) / 100;
        string amountRefund = amountRefundInFloat.ToString("0.00");
        
        Response.Write("<table>");
		Response.Write("<tr><td>refund_fee: </td><td>$" + amountRefund + "</td></tr>");
        Response.Write("<tr><td>total_fee: </td><td>$" + amountPaid + "</td></tr>");
		Response.Write("<tr><td>pay_channel:</td><td>" + result["pay_channel"] + "</td></tr>");
		Response.Write("<tr><td>refund_status: </td><td>" + result["refund_status"] + "</td></tr>");
		Response.Write("<tr><td>message: </td><td>" + result["message"] + "</td></tr>");
		Response.Write("</table>");
    }
    else 
    {
        Response.Write("Refund Failed. <br/>");
		Response.Write("message:" + result["message"]);
    }
%>           
           
      </ul>
    </div>

 
</div>

</body>

</html>