﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orderList.aspx.cs" Inherits="MotionPayDemo.orderList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
</script>	

  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span>Sample Order List Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
      <span style="float:right;"><a href="index.aspx"><font style="font-size:15px;font-weight:bold;color:#f60;">Payment Page</font></a></span> 
    </div>
</div>
  
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Order List</label></h6>
  	  <ul class="sel_list">
<%
    int counter = 0;
    string line;

    string logfile = Server.MapPath("~/log/" + DateTime.Now.ToString("yyyyMMdd") + "/motionpayOrders.txt");

    try
    {
        System.IO.StreamReader file =
            new System.IO.StreamReader(logfile);
        Response.Write("<table>\n");
        while ((line = file.ReadLine()) != null)
        {
            int pos = line.IndexOf("outTradeNo"); 
            if(pos >= 0)
            {
                List<String> elements = line.Split(',').ToList();
                if ( elements.Count > 1 )
                {
                    string outTradeNoEle = elements[0];
                    string paymentTypeEle = elements[1];

                    string outTradeNo = outTradeNoEle.Replace("outTradeNo:", "").Trim(new char[] { '\uFEFF', '\u200B' });
                    string paymentType = paymentTypeEle.Replace("paymentType:", "").Trim(new char[] { '\uFEFF', '\u200B' });
                    Response.Write("<tr><td>");
                    Response.Write("OrderId: <a href='orderDetail.aspx?orderId=" + outTradeNo + "&paymentType=" + paymentType + "'>" + outTradeNo + "</a>");
                    Response.Write("</td></tr>");
                }
                counter++;                
            }
        }
        Response.Write("</table>\n");
        file.Close();
    }
    catch (Exception ee) {
        Response.Write(ee);
    }

%>           
           
      </ul>
    </div>

 
</div>

</body>

</html>
