﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="callback.aspx.cs" Inherits="MotionPayDemo.callback" %>
<%@ Import Namespace="MotionPayDemo.Pay" %>
<%@ Import Namespace="TuYu.Common" %>

<%
    string responseStr = "fail";
    string postData = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
    List<string> values = new List<string>();
    List<string> keys = new List<string>();
    List<string> properties = new List<string>();
    string signServer = "";

    Log.WriteLog("motionpay", "callback data is:" + postData);
    string pattern = @"\""(?<key>[^\""]+)\""\:\""?(?<value>[^\"",}]+)\""?\,?";
    foreach (Match m in Regex.Matches(postData, pattern))
    {
        if (m.Success)
        {
            string key = m.Groups["key"].Value;
            if(key == "sign")
            {
                signServer = m.Groups["value"].Value;
                continue;
            }
            values.Add(m.Groups["value"].Value);
            keys.Add(m.Groups["key"].Value);
            properties.Add(m.Groups["key"].Value + "=" + m.Groups["value"].Value);
        }
    }

    string queryStr = String.Join("&", properties.ToArray());
    Log.WriteLog("motionpay", "queryStr is:" + queryStr);
    MotionPayOnline onlineAPI = new MotionPayOnline();
    string signStr = onlineAPI.getSignForVerify(queryStr);
    Log.WriteLog("motionpay", "signStr is:" + signStr);
    Log.WriteLog("motionpay", "signServer is:" + signServer);
    
    if (signStr == signServer)
    {
        Response.Write("{\"code\":\"0\",\"message\":\"success\"}");
        // you can put your business logic here as this order has been paid successfully.
    }
    else
    {
        Response.Write(responseStr);
    }
%>