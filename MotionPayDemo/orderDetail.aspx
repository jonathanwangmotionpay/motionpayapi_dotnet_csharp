﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orderDetail.aspx.cs" Inherits="MotionPayDemo.orderDetail" %>
<%@ Import Namespace="MotionPayDemo.Pay" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Order List Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>

<style>
table, td, th {
    text-align: left;
}
</style>
  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span>Sample Order Detail Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>      
      <span style="float:right;"><a href="orderList.aspx"><font style="font-size:15px;font-weight:bold;color:#f60;">Order List</font></a></span> 
    </div>
</div>


  <form id="payment" action="orderRefund.aspx" method="post" >
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Order Detail Sample Page</label></h6>
  	  <ul class="sel_list">

<%
    string queryResult = "not pay or not exist!";
    String outTradeNo = Request["orderId"];
    
    Hashtable requestParams = new Hashtable();
    MotionPayOnline onlineAPI = new MotionPayOnline();
    requestParams.Add("out_trade_no", outTradeNo);
    Hashtable result = onlineAPI.doQuery(requestParams);
    string trade_status = (string)result["trade_status"];
    if (trade_status == "SUCCESS")
    {
        queryResult = "paid";
        string amountPaidInCents = (string)result["total_fee"];
        if (amountPaidInCents == null || amountPaidInCents.Length == 0)
        {
            amountPaidInCents = "0";
        }
        float amountInFloat = float.Parse(amountPaidInCents) / 100;
        string amountPaid = amountInFloat.ToString("0.00");

        Response.Write("<table>");
        Response.Write("<tr><td>exchange_rate:</td><td>" + result["exchange_rate"] + "</td></tr>");
        Response.Write("<tr><td>currency_type:</td><td>" + result["currency_type"] + "</td></tr>");
        Response.Write("<tr><td>total_fee: </td><td>$" + amountPaid + "</td></tr>");
		Response.Write("<tr><td>pay_channel:</td><td>" + result["pay_channel"] + "</td></tr>");
        Response.Write("<tr><td>trade_status: </td><td>" + result["trade_status"] + "</td></tr>");
        Response.Write("<tr><td>user_identify: </td><td>" + result["user_identify"] + "</td></tr>");
        Response.Write("<tr><td>Refund Amount:  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>");
		Response.Write("<td><input type='text' name='refundAmount' value='" + amountPaid + "' size='5'/> &nbsp;");
		Response.Write("<input type='hidden' name='totalAmount' value='" + amountPaid + "'>");
        Response.Write("<input type='hidden' name='orderId' value='" + outTradeNo + "'>");
        Response.Write("<input type='submit' name='refund' value='Refund'></td></tr>");        
        Response.Write("</table>");

    }
    else
    { 
        Response.Write(queryResult);
    }
%>           
             
      </ul>
    </div>
  </form>
 
</div>

</body>

</html>

