﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="paymentNotify.aspx.cs" Inherits="MotionPayDemo.paymentNotify" %>
<%@ Import Namespace="MotionPayDemo.Pay" %>

<%
    string queryResult = "not pay";
    Hashtable requestParams = new Hashtable();
    String orderId = Request["orderId"];

    MotionPayOnline onlineAPI = new MotionPayOnline();
    requestParams.Add("out_trade_no", orderId);
    Hashtable result = onlineAPI.doQuery(requestParams);
    string trade_status = (string) result["trade_status"];
    if (trade_status == "SUCCESS")
    {
        queryResult = "paid";
    }
    Response.Write(queryResult);
%>